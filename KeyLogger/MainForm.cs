﻿using System.Windows.Forms;

namespace KeyLogger
{
    public partial class MainForm : Form
    {
        private KeyLogger keyLogger;
        public MainForm()
        {

            InitializeComponent();
            keyLogger = new KeyLogger();
            keyLogger.SymRecieved += s => logTextBox.Text += s.ToString();
        }
        private void MainFormClosing(object sender, FormClosingEventArgs e)
        {
            keyLogger.Unhook();
        }
    }
}
